/**
 * All the reflection based configuration classes.
 *
 * @Author: Cayde
 */
package net.cazzar.corelib.configuration.annotations;